<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Blis Media</title>
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } 
ul.nav a { zoom: 1; }  
</style>
<![endif]-->
<link href="{$global_url}css/blis_followon_black.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
</script>
</head>
<body>
<div class="container">  <div class="content">
	<div id="topnavarea">
		<ul>
			<li><a href="http://blismedia.com/index.html"><img src="{$global_url}images/home_topnav.png" width="41" height="30" alt="Home"></a></li>
		<!--	<li class="what"><a href="http://blismedia.com/who_we_are.html"><img src="{$global_url}images/grey_whoarewe_topnav.png" width="86" height="30" alt="Blis Media Who are we"></a></li> --> 
			<li><a href="http://blismedia.com/what_we_do.html"><img src="{$global_url}images/greyproducts_topnav.png" width="62" height="30" alt="Products"></a></li> 
		<!--	<li><a href="http://blismedia.com/blis_analytics.html"><img src="{$global_url}images/grey_analytics_topnav.png" width="65" height="30" alt="Blis Media Analytics"></a></li> -->
			<li><a href="http://blismedia.com/meet_clients.html"><img src="{$global_url}images/meet_topnav.png" width="110" height="30" alt="Meet our clients"></a></li>
			<li><a href="http://blismedia.com/have_coffee_board.html"><img src="{$global_url}images/coffee_topnav.png" width="127" height="30" alt="Let's have a coffee"></a></li>
		</ul>
	</div>

	<div class="main">
		<div class="subtitle">
			{if $download eq 'yes'}
				<center>Sorry, the file you are looking is not found on the server. Please contact whoever sent you the link</center>
			{else}
				{if $smarty.session.loggedin neq "yes"}
					<form action="{$global_url}index.php" method="POST">
						{if $l_failed eq 'yes'}
							<center>Login failed</center>
						{/if}
						Username : <input type="text" name="login" value="{$smarty.post.login}">
						Password : <input type="password" name="pass">
						<input type="submit" value="Login">
					</form>
				{else}
					<span style="font-size:12px;">Logged as <a href="{$global_url}index.php?do=logout">{$smarty.session.username}</a><br/><br/></span>
					<form action="{$global_url}index.php" method="POST" enctype="multipart/form-data">
						<span style="font-size:12px;">Company Name : <input type="text" name="email"><br/><br/>
						File : <input type="file" name="file"><input type="hidden" name="upload" value="ok"/> OR URL: <input type="text" name="url" value="" />
						<input type="submit" value="Upload"><br/>
						<font color="red">Please ensure any link that you create is not distributed to more than one company.</font></span>
					</form><br/>
					<span style="font-size:12px;">{html_table loop=$data cols="Email, File Name,File ID,Upload Date/Time" table_attr='border="0", cellpadding="3", cellspacing="3"'}</span>
					<span style="color:red; font-size:12px;">File(s) will be stored for 4 weeks!</span>
				{/if}
			{/if}
		</div>
	</div>
</div>
